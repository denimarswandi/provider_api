import 'package:flutter/material.dart';
import 'package:provider_api/ui/hello.dart';
import 'package:provider/provider.dart';
import 'package:provider_api/prov/siswa_prov.dart';

void main() {
  runApp(MultiProvider(
    providers: [ChangeNotifierProvider(create: (_) => SiswaProv())],
    child: MaterialApp(home: Hello()),
  ));
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_api/model/siswa.dart';
import 'package:provider_api/prov/siswa_prov.dart';
import 'package:provider_api/ui/add.dart';

class Hello extends StatefulWidget {
  @override
  _HelloState createState() => _HelloState();
}

class _HelloState extends State<Hello> {
  List<Siswa> siswa;

  @override
  Widget build(BuildContext context) {
    final SiswaProv siswaprov = Provider.of<SiswaProv>(context);
    siswaprov.getAll();
    siswa = context.watch<SiswaProv>().siswaAll;
    print(context.watch<SiswaProv>().siswaAll);
    return Scaffold(
      appBar: AppBar(
        title: Text("Halooo"),
        actions: [
          GestureDetector(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Add()));
              },
              child: Icon(Icons.add, color: Colors.white))
        ],
      ),
      body: Container(
        child: siswa == null
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: siswa.length,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                      child: Row(
                    children: [
                      Text(siswa[index].nama),
                      RaisedButton(
                          onPressed: () {
                            siswaprov.delete(siswa[index].id).then((value) {
                              if (value) {
                                context.read<SiswaProv>().setNull();
                              }
                            });
                          },
                          child: Text("Hapus"))
                    ],
                  ));
                },
              ),
      ),
    );
  }
}

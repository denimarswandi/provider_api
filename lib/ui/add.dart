import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_api/model/siswa.dart';
import 'package:provider_api/prov/siswa_prov.dart';

class Add extends StatefulWidget {
  @override
  _AddState createState() => _AddState();
}

class _AddState extends State<Add> {
  Siswa siswa;
  TextEditingController _nama = TextEditingController();
  TextEditingController _alamat = TextEditingController();
  TextEditingController _t_lahir = TextEditingController();
  TextEditingController _jl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    SiswaProv siswaProv = Provider.of<SiswaProv>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Tambah"),
      ),
      body: Container(
          child: ListView(
        children: [
          TextField(
            autocorrect: false,
            decoration: InputDecoration(labelText: "Nama"),
            controller: _nama,
          ),
          TextField(
            autocorrect: false,
            decoration: InputDecoration(labelText: "Alamat"),
            controller: _alamat,
          ),
          TextField(
            autocorrect: false,
            decoration: InputDecoration(labelText: "Tanggal Lahir"),
            controller: _t_lahir,
          ),
          TextField(
            autocorrect: false,
            decoration: InputDecoration(labelText: "Kelamin"),
            controller: _jl,
          ),
          RaisedButton(
            onPressed: () {
              siswa = Siswa(
                  id: 0,
                  nama: _nama.text.toString(),
                  alamat: _alamat.text.toString(),
                  t_lahir: _t_lahir.text.toString(),
                  jl: _jl.text.toString());
              siswaProv.save(siswa).then((value) {
                if (value) {
                  context.read<SiswaProv>().setNull();
                  Navigator.pop(context);
                } else {
                  print("Uuuups");
                }
              });
            },
            child: Text("Simpan"),
          )
        ],
      )),
    );
  }
}

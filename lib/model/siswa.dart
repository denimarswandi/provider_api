import 'dart:convert';

class Siswa {
  final int id;
  final String nama;
  final String alamat;
  final String t_lahir;
  final String jl;

  Siswa({this.id, this.nama, this.alamat, this.t_lahir, this.jl});

  factory Siswa.fromJson(Map<String, dynamic> json) {
    return Siswa(
        id: json['id'],
        nama: json['nama'],
        alamat: json['alamat'],
        t_lahir: json['t_lahir'],
        jl: json['jl']);
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "nama": nama,
      "alamat": alamat,
      "t_lahir": t_lahir,
      "jl": jl
    };
  }
}

List<Siswa> siswaFromJsonAll(String jsonData) {
  final data = json.decode(jsonData);
  return List<Siswa>.from(data.map((item) => Siswa.fromJson(item)));
}

dataToJson(Siswa data) {
  final json_ = data.toJson();
  return json.encode(json_);
}

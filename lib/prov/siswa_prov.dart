import 'package:flutter/foundation.dart';
import 'package:provider_api/model/siswa.dart';
import 'dart:async';
import 'package:http/http.dart' as http;

class SiswaProv with ChangeNotifier {
  static const url = "https://api-v2.pondokdiya.id";

  List<Siswa> _siswaAll;

  get siswaAll => _siswaAll;

  void setAll(List<Siswa> siswa) {
    _siswaAll = siswa;
    notifyListeners();
  }

  void setNull() {
    _siswaAll = null;
    notifyListeners();
  }

  Future getAll() async {
    final response = await http.get('$url/siswa');
    if (response.statusCode == 200) {
      setAll(siswaFromJsonAll(response.body));
    }
  }

  Future<bool> save(Siswa data) async {
    final response = await http.post('$url/add-siswa',
        headers: {"content-type": "application/json"}, body: dataToJson(data));
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> delete(int id) async {
    final response = await http.delete('$url/delete-siswa/$id',
        headers: {"content-type": "application/json"});
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }
}
